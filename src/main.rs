extern crate core;

use std::collections::btree_set::BTreeSet;
use std::mem;

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
struct TravelState {
    /// hours spent within the blizzard [0, 7 or 10]
    hours: u8,
    /// total number of times PC has failed the CON check [0, 5]
    fails: u8,
    /// accumulated hit point damage from the blizzard [0, 18] or hit points of character
    damage: u8,
}

impl TravelState {
    /// Initial state for characters
    const INIT: TravelState = TravelState {
        hours: 0,
        fails: 0,
        damage: 0,
    };
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
struct Character {
    /// CON score; game limits between [3, 18]
    con: u8,
    /// hit points; may range from [1, 20]
    hp: u8,
}

impl Character {
    /// Return true if damage taken by this point in the travel meets or exceeds the character
    /// hit points (i.e. the damage is sufficient to kill them)
    fn is_dead(self, state: TravelState) -> bool {
        state.damage >= self.hp
    }

    /// Compute the transition probability between the pair (from, to) travel states.
    /// Return 0.0 if there is no transition possible between the two states.
    fn prob(self, from: TravelState, to: TravelState) -> f32 {
        let success_prob = if from.fails < 6 {
            (self.con as f32 * (5.0 - from.fails as f32)) / 100.0
        } else {
            0.0
        };

        if from.hours + 1 == to.hours {
            match ((to.fails - from.fails), (to.damage - from.damage)) {
                (0, 0) => success_prob,
                (1, h) if h == to.hours => 1.0 - success_prob,
                _ => 0.0,
            }
        } else if from == to {
            // final state?
            1.0
        } else {
            0.0
        }
    }
}

/// Return a vector of travel states and a Markov transition matrix, representing a character
/// travelling through the blizzard to Karlensburg. The sparse matrix values are transition
/// probabilities, represented as float in the range 0 to 1.0 (although zeroes are not stored
/// explicitly in the matrix). The indices of the travel states correspond to the row and column
/// indices of the transition matrix.
///
/// Every state leads to two additional states until the character has finished the journey, after
/// either seven or ten transitions, in which case the states transition to themselves. For a seven
/// hour journey, there will be strictly less than 1 + 2 + 4 + ... + 128 = 255 states. For ten
/// hours, 1023.
fn build_transition_matrix(
    character: &Character,
    hours: u8,
) -> (Vec<TravelState>, sprs::CsMatI<f32, usize>) {
    // Since multiple transitions may lead to the same state, prev_states/next_states is a set
    // rather than a list so we avoid duplicates.
    let mut prev_states: BTreeSet<TravelState> = BTreeSet::new();
    prev_states.insert(TravelState::INIT);
    let mut next_states: BTreeSet<TravelState> = BTreeSet::new();

    let mut vertices: Vec<TravelState> = Vec::new();
    vertices.push(TravelState::INIT);
    let mut edges: Vec<(TravelState, TravelState)> = Vec::new();

    for hour in 1..=hours {
        for prev_state in &prev_states {
            let success = TravelState {
                hours: hour,
                fails: prev_state.fails,
                damage: prev_state.damage,
            };
            let fail = TravelState {
                hours: hour,
                fails: prev_state.fails + 1,
                damage: prev_state.damage + hour,
            };

            vertices.push(success);
            edges.push((*prev_state, success));
            next_states.insert(success);

            vertices.push(fail);
            edges.push((*prev_state, fail));
            next_states.insert(fail);
        }

        mem::swap(&mut prev_states, &mut next_states);
        next_states.clear();
    }
    // for the final hour, the states should transition to themselves
    for prev_state in &prev_states {
        edges.push((*prev_state, *prev_state));
    }
    prev_states.clear();

    vertices.sort();
    vertices.dedup();

    let mut mat = sprs::TriMatBase::with_capacity((vertices.len(), vertices.len()), edges.len());
    for (src, dst) in edges {
        mat.add_triplet(
            vertices.binary_search(&dst).unwrap(),
            vertices.binary_search(&src).unwrap(),
            character.prob(src, dst),
        );
    }

    (vertices, mat.to_csr())
}

/// Compute the survival probability (probability of surviving alive, probability of dying in the
/// blizzard) for a character travelling the specified number of hours. The `states` and `transition`
/// arguments should be the output of the `build_transition_matrix` function.
fn survival(
    hours: u8,
    character: Character,
    states: Vec<TravelState>,
    transition: sprs::CsMatI<f32, usize>,
) -> (f32, f32) {
    let initial = sprs::CsVec::new(states.len(), vec![1], vec![1.0f32]);

    // Within a Markov chain, the probability after n transitions of a transition matrix A
    // is equal to A**n * x, where x is in the initial state column vector.
    let mut m = transition;
    for _ in 0..hours {
        m = &m * &m;
    }
    m = &m * &initial.col_view();

    // Why treat the two probabilities as independent? This is a way to verify the process has
    // resulted in values where p and not p is equal to 1 (or sufficiently close).
    let mut alive_prob = 0.0;
    let mut dead_prob = 0.0;
    for (prob, (row, _)) in m.iter() {
        if character.is_dead(states[row]) {
            dead_prob += prob;
        } else {
            alive_prob += prob;
        }
    }

    (alive_prob, dead_prob)
}

fn main() {
    println!("hours,CON,hp,alive,dead,alive + dead");
    for con in 3..=18 {
        for hp in 1..=20 {
            for hours in vec![7, 10] {
                let character = Character { con, hp };
                let (states, mat) = build_transition_matrix(&character, hours);
                let (alive, dead) = survival(hours, character, states, mat);
                println!("{},{},{},{},{},{}", hours, con, hp, alive, dead, alive + dead);
            }
        }
    }
}
