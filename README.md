# Surviving the Blizzard: Escape to Karlensburg

Uses a Markov Chain to compute the probabilities of surviving a trip to
Karlensburg during the Blizzard, per the _Blood Brothers 2_ adventure
"Chateau of Blood".

